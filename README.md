# HTR Firebird
HTR FIREBIRD FRAMEWORK 3.0 - Copyright (C) <2015 BRUNO MONTEIRO> 
Framework PHP e MVC para agilizar o desenvolvimento de Aplicativos Web 
bruno.monteirodg@gmail.com

Para Criar Módulo (Controller, Model e View) use o HTR Assist

instalando via composer
```bash
$ composer create-project phbsis/htr-framework teste
```
